require 'test_helper'

class EntryTest < ActiveSupport::TestCase
  def setup
    @entry = Entry.new(content: 'Hello world!')
  end

  test 'content should be present' do
    @entry.content = nil
    assert @entry.invalid?
  end
end
