class InstagramController < ApplicationController

  def oauth_connect
    redirect_to Instagram.authorize_url(:redirect_uri => $instagram_callback_url)
  end

  def oauth_callback
    response = Instagram.get_access_token(params[:code], :redirect_uri => $instagram_callback_url)
    session[:access_token] = response.access_token
    redirect_to root_url
  end

end