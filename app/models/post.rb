class Post
  include ActiveModel::Model
  attr_accessor :created_at, :content, :category, :url, :media

  def to_partial_path
    'feed/post'
  end
end