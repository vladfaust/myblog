require 'test_helper'
include Kaminari
include FeedHelper

class SiteLayoutTest < ActionDispatch::IntegrationTest

  def setup

  end

  test 'home' do
    get root_path
    assert_template 'feed/index'
    assert_template 'feed/_post'

    get_feed.each do |p|
      assert_match p.content, response.body
      assert_match p.url, response.body
    end
  end

end
