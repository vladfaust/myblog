module FeedHelper

  # Returns united feed
  def get_feed (length = 15)
    feed = []

    entries = Entry.all.limit(length).order(created_at: :desc).to_a

      # p 'Started fetching tweets.'
    tweets  = Rails.env != 'test' ? $twitter_client.user_timeline($twitter_user_id, count: length).to_a : []
      # p 'Done fetching tweets. Fetched: ' + tweets.count.to_s + '.'

      # p 'Started fetching gramms.'
    gramms = Rails.env != 'test' ? $instagram_client.user_recent_media($instagram_user_id, count: length, access_token: $instagram_access_token).to_a : []
      # p 'Done fetching gramms. Fetched: ' + gramms.count.to_s + '.'

    while length > 0 do
      entry_time = !entries.first.nil? ? entries.first.created_at : Time.at(0)
      tweet_time = !tweets.first.nil? ? tweets.first.created_at : Time.at(0)
      gramm_time = !gramms.first.nil? ? Time.at(gramms.first.created_time.to_i) : Time.at(0)

      if entry_time == 0 && tweet_time == 0 && gramm_time == 0
        return feed
      end

      if entry_time >= tweet_time && entry_time >= gramm_time
        feed.append to_post(entries.first)
        entries.shift
      elsif tweet_time >= entry_time && tweet_time >= gramm_time
        feed.append to_post(tweets.first)
        tweets.shift
      elsif gramm_time >= entry_time && gramm_time >= tweet_time
        feed.append to_post(gramms.first)
        gramms.shift
      end

      length -= 1
    end

    feed
  end

  # Converts anything to universal Post class
  def to_post (converted)
    if converted.class == Entry
      # p 'Url = ' + entry_url(converted)
      Post.new(
          category: 0,
          content: converted.content,
          created_at: converted.created_at,
          url: entry_url(converted))
    elsif converted.class == Twitter::Tweet
      Post.new(
          category: 1,
          content: Twitter::Autolink.auto_link(converted.text, username_include_symbol: true, target_blank: true),
          created_at: converted.created_at,
          url: converted.uri.to_s,
          media: converted.try(:media) == [] ? nil : converted.media)
    elsif !converted.try(:images).nil? # It should be instagram
      Post.new(
            category: 2,
            created_at: Time.at(converted.created_time.to_i),
            content: instagram_content(converted.caption.text),
            url: converted.link,
            media: convert_instagram_media(converted))
    else
      raise 'Wrong class (' + converted.class.to_s + ')!'
    end
  end

  # Whether to add only one (first) media
  $only_one_media = true

  # Returns the link to a post
  def get_post_link (post)
    # p 'Link url: '+ URI::parse(post.url).host
    link_to URI::parse(post.url).host, post.url, target: '_blank'
  end

  # Returns the post's avatar
  def get_post_avatar (post)
    if post.category == 0 # Plain post
      content_tag :div, '', class: 'pull-left avatar empty'
    elsif post.category == 1 # Twitter
      link_to image_tag($twitter_user.profile_image_uri(:bigger), alt: 'Go to my Twitter', lazy: true), 'https://twitter.com/vladfaust', class: 'pull-left avatar', target: '_blank'
    elsif post.category == 2 # Instagram
      link_to image_tag($instagram_user.profile_picture, alt: 'Go to my Instagram', lazy: true), 'https://instagram.com/vladfaust', class: 'pull-left avatar', target: '_blank'
    end
  end

  # Return the post's content
  def get_post_content (post)
    result = sanitize((post.content ||= '').html_safe)

    unless post.media.nil?
      media = tag 'hr'

      if $only_one_media
        media += get_media_content(post.media.first)
      else
        post.media.each { |m| media += get_media_content(m) }
      end

      result += content_tag :div, media.html_safe, class: 'media'
    end

    result
  end

  # Returns the media tag depending on it's type
  def get_media_content (media)
    if media.class == Twitter::Media::Photo
      link_to image_tag(media.media_url, lazy: true), media.url.to_s, target: '_blank'
    elsif media.class == Hash && !media[:image_path].nil?
        link_to image_tag(media[:image_path], lazy: true), media[:url], target: '_blank'
    end
  end

  # Check instagram connection
  def instagram
    $instagram_client = Instagram.client(:access_token => $instagram_access_token)
    $instagram_user = $instagram_client.user
  end

  # Converts instagram media to valid media
  def convert_instagram_media (post)
    [{ image_path: post.images.standard_resolution.url, url: post.link }]
  end

  def instagram_content (content)
    content = Twitter::Autolink.auto_link(content, username_include_symbol: true, target_blank: true)
    content.gsub!('https://twitter.com/#!/search?q=%23', 'https://instagram.com/explore/tags/')
    content.gsub!('https://twitter.com/', 'https://instagram.com/')
    content
  end
end
