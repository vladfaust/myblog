class FeedController < ApplicationController

  include FeedHelper

  # Shows feed
  def index
    # Try to connect to instagram.
    instagram
    @feed = get_feed
    respond_to do |format|
      format.html
      format.js
    end
  end

end
