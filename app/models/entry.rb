class Entry < ActiveRecord::Base
  validates :content, presence: true
end
